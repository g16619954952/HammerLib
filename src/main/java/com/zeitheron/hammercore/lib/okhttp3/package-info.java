/** An HTTP+HTTP/2 client for Android and Java applications. */
@com.zeitheron.hammercore.lib.okhttp3.internal.annotations.EverythingIsNonNull
package com.zeitheron.hammercore.lib.okhttp3;
